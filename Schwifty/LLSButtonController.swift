//
//  LLSButtonController.swift
//  Schwifty
//
//  Created by Steven T. Norris on 2/8/17.
//  Copyright © 2017 FortyAU. All rights reserved.
//

import UIKit

class LLSButtonController: UIViewController {

    @IBOutlet var pressMeBtn:UIButton!
    @IBOutlet var pressMeLbl:UILabel!
    
    let messages = ["Who you pressin?", "Yo, for realz. Stop pressin!", "You think I'm playin', but I'm not.", "With the pressin already man. For real."]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func pressMeBtnTouch(){
        
        let index = arc4random_uniform(UInt32(self.messages.count))
        self.pressMeLbl.text = messages[Int(index)]
        
    }
    
    @IBAction func tableViewBtnTouch(){
        let tableControl = LLSTableController(nibName: "LLSTableView", bundle: nil)
        self.present(tableControl, animated: true, completion: nil)
    }


}

