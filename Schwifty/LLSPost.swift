//
//  LLSPost.swift
//  Schwifty
//
//  Created by Steven T. Norris on 3/29/17.
//  Copyright © 2017 FortyAU. All rights reserved.
//

import Foundation
import Gloss

class LLSPost: Decodable, Equatable{
    
    let id:Int
    let userId:Int?
    let title:String
    let body:String?
    
    //--------------------------
    //Mark: - Deserialization
    //--------------------------
    
    required init?(json: JSON){
        self.userId = "userId" <~~ json
        self.id = ("id" <~~ json)!
        self.title = ("title" <~~ json)!
        self.body = "body" <~~ json
    }
    
}

//--------------------------
//Mark: - Equatable Methods
//--------------------------

func == (lhs:LLSPost, rhs:LLSPost) -> Bool {
    return lhs.id == rhs.id
}


