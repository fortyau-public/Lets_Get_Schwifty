//
//  LLSTableCell.swift
//  Schwifty
//
//  Created by Steven T. Norris on 3/8/17.
//  Copyright © 2017 FortyAU. All rights reserved.
//

import UIKit

class LLSTableCell: UITableViewCell {

    @IBOutlet var titleLbl:UILabel!
    @IBOutlet var colorVw:UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }

}
