//
//  LLSTableController.swift
//  Schwifty
//
//  Created by Steven T. Norris on 3/8/17.
//  Copyright © 2017 FortyAU. All rights reserved.
//

import UIKit
import Alamofire

class LLSTableController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet var tableVw:UITableView!
    @IBOutlet var backBtn:UIButton!
    @IBOutlet var coolLbl:UILabel!
    
    var posts:[LLSPost] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableVw.register(UINib(nibName: "LLSTableCell", bundle: nil), forCellReuseIdentifier: "TableCell")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        Alamofire.request(
            "https://jsonplaceholder.typicode.com/posts",
            method: HTTPMethod.get,
            parameters: nil,
            encoding: JSONEncoding.default,
            headers: nil
            )
            .validate()
            .responseJSON{response in
                
                switch response.result {
                    
                case .success(let JSON):
                    
                    self.posts = [LLSPost].from(jsonArray: JSON as! [[String:Any]])!
                    self.tableVw.reloadData()
                    
                case .failure(let error):
                    
                    print(error)
                    
                }
                
            }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //TableView Delegate Methods
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let post = self.posts[indexPath.row]
        self.coolLbl.text = post.body
    }
    
    //TableView Datasource Methods
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableVw.dequeueReusableCell(withIdentifier: "TableCell") as! LLSTableCell
        let post = self.posts[indexPath.row]
        cell.titleLbl.text = post.title
        cell.colorVw.backgroundColor = UIColor.blue
        
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.posts.count
    }
    
    
    //Button Actions
    @IBAction func backBtnTouch(){
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
    


}
